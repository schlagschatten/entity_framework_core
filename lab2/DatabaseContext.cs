using System;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using laba2.DataClasses;

namespace laba2
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Calls> Calls { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Person> Persons { get; set; }
private string connectionString=null;
        public DatabaseContext(string connectionString)
        {
            this.connectionString = connectionString;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(this.connectionString);
        }
    }
}