﻿using laba2.DataClasses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace laba2
{
    internal class Program
    {
        #region Private Methods

        private static void Main(string[] args)
        {
            using (var db =
                new DatabaseContext(ConfigurationManager.ConnectionStrings["AzureLibrary"].ConnectionString))
            {
                var usersInfoList = db.Persons.Select(user =>
                    new
                    {
                        user.id,
                        user.first_name,
                        user.second_name,
                        user.last_name,
                        user.phone_number,
                        user.address.city,
                        user.address.street,
                        user.address.house_number,

                        cityCount = user.calls.Select(x => x.city)
                            .Distinct()
                            .Count()
                    }
                ).GroupBy(user => user.id);

                foreach (var group in usersInfoList)
                {
                    foreach (var user in group)
                    {
                        Console.WriteLine(
                            $"First name: {user.first_name}; " +
                            $" Second name: {user.second_name};" +
                            $" Last name {user.last_name}, " +
                            $"Phone number: {user.phone_number};" +
                            $" City: {user.city};" +
                            $" Street {user.street};" +
                            $" House number {user.house_number}; " +
                            $"Count city: {user.cityCount}");
                    }
                }

                var userInfos2 =
                    from user in db.Persons
                    let calls = user.calls == null ? user.calls : new List<Calls>()
                    select new
                    {
                        user.first_name,
                        user.last_name,
                        calls,
                    };

                foreach (var user in userInfos2)
                {
                    Console.WriteLine($"First name: {user.first_name};  Second name: {user.last_name}");
                    
                    var calls = user.calls.Select(x=> new {x.city.code, x.city.city});
                    if (user.calls != null)
                    {
                        foreach (var userCall in calls)
                        {
                            Console.WriteLine(
                                $"Code: {userCall.code}; City: {userCall.city}, ");
                        }
                    }
                }
            }
        }

        #endregion Private Methods
    }
}